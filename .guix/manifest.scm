(specifications->manifest
 (list "emacs"
       "emacs-org"
       "texlive"
       "texlive-biber"
       "graphviz"
       "inkscape"
       "python@3"
       "python-pygments"
       "git"
       "bash"
       "coreutils"
       "which"))
