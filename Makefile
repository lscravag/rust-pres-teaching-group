##
# Project Title
#
# @file
# @version 0.1
OUTDIR=./public
TIMEMACHINE=guix time-machine
CHANNEL=-C ./.guix/channels.scm
MANIFEST=-m ./.guix/manifest.scm
EMACSPUB=emacs --batch --no-init-file --load publish.el --eval '(org-publish "slides")'

.PHONY: all

all: public/slides.pdf

$(OUTDIR)/slides.pdf: slides.org beamerthemeinria.sty
	$(TIMEMACHINE) $(CHANNEL) -- shell --container $(MANIFEST) -- $(EMACSPUB)
	cp slides.pdf $@

# end
