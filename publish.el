;;; publish.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Lana Huong Scravaglieri
;;
;; Author: Lana Huong Scravaglieri <lana@scravaglieri.net>
;; Maintainer: Lana Huong Scravaglieri <lana@scravaglieri.net>
;; Created: April 09, 2024
;; Modified: April 09, 2024
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/lscravag/publish
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
(require 'org)
;; Global publishing functions
(require 'ox-publish)
;; LaTeX publishing functions
(require 'ox-latex)
;; Support for citations
(require 'oc)
(require 'oc-biblatex)

(require 'ox-beamer)
(add-to-list 'org-beamer-environments-extra
             '("generic" "g" "\\\\begin{generic}{%h}" "\\\\end{generic}"))


;; Enable Babel code evaluation.
(setq org-export-babel-evaluate t)

;; Do not prompt for code block evaluation.
(setq org-confirm-babel-evaluate nil)

;; Force publishing of unchanged files.
(setq org-publish-use-timestamps-flag nil)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((dot . t)))

;; Override the default LaTeX publishing command.
(setq org-latex-pdf-process
      (list "pdflatex --shell-escape -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f"
            "pdflatex --shell-escape -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f"
            "pdflatex --shell-escape -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f"
            ))


;; Use BibLaTeX citation processor by default.
(setq org-cite-export-processors '((t biblatex)))

;; Configure LaTeX to use 'minted' for code block export.
(setq org-latex-packages-alist '())
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("linenos = false") ("breaklines")
        ("style = pastie")))

;; Load additional LaTeX packages allowing us to:
;;   - use scalable vector graphics (without exporting them through LaTeX),
(add-to-list 'org-latex-packages-alist '("inkscapelatex = false" "svg"))
;;   - use Helvetica as the default sans-serif font,
                                        ;(add-to-list 'org-latex-packages-alist '("" "helvet"))
;;   - use smileys,
(add-to-list 'org-latex-packages-alist '("" "tikzsymbols"))
;;   - use the dedicated IFPEN Beamer theme
(add-to-list 'org-latex-packages-alist '("" "beamerthemeinria"))

;; (defun my-beamer-maketitle-filter (output backend info)
;;   (if (string= backend "beamer")
;;       (replace-regexp-in-string "\\\\maketitle"
;;                                 "\\\\frame[plain]{\\\\titlepage}"
;;                                 output)
;;     output))

;; (defun my-beamer-ulem-filter (output backend info)
;;   (if (string= backend "beamer")
;;       (replace-regexp-in-string "\\\\usepackage[normalem]{ulem}"
;;                                 ""
;;                                 output)
;;     output))

;; (defun my-beamer-frame-generic-filter (output backend info)
;;   (if (string= backend "beamer")
;;       (replace-regexp-in-string "{frame}"
;;                                 "{generic}"
;;                                 output)
;;     output))


;; (add-to-list 'org-export-filter-final-output-functions
;;              'my-beamer-frame-generic-filter)
;; (add-to-list 'org-export-filter-final-output-functions
;;              'my-beamer-ulem-filter)
;; (add-to-list 'org-export-filter-final-output-functions
;;              'my-beamer-maketitle-filter)

;; Define publishing targets.
(setq org-publish-project-alist
      (list
       (list "slides"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["slides.org"]
             :publishing-function '(org-beamer-publish-to-pdf)
             :publishing-directory "./public")))

(provide 'publish)

;;; publish.el ends here
