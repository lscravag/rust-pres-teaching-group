#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [aspectratio=169,small]
#+LANGUAGE: en
#+BEAMER_THEME: inria
#+OPTIONS: toc:nil H:2 author:nil email:nil ^:{}
# #+BEAMER_FRAME_LEVEL: 1
#+TITLE: Are we (teaching) Rust yet?
#+SUBTITLE: \includegraphics[scale=0.2]{rustacean-flat-happy.png}
#+DATE: May 16, 2024
#+BEAMER_HEADER: \author{Lana Scravaglieri}
#+BEAMER_HEADER: \institute{IFPEN, R11 - Inria STORM}
#+MACRO: pause @@beamer:\pause@@
#+MACRO: arrow @@beamer:$\rightarrow$@@
#+MACRO: newline @@beamer:\\@@
#+MACRO: winkey @@beamer:\Winkey@@
#+MACRO: bold @@beamer:\textbf{$1}@@

# #+LaTeX_HEADER: \usepackage[T1]{fontenc}
# #+LaTeX_HEADER: \usepackage[latin1]{inputenc}
#+LaTeX_HEADER: \usepackage{stackengine}
#+LaTeX_HEADER: \usepackage{bookmark}
#+LaTeX_HEADER: \renewcommand\stackalignment{l}

# for \specialrule command
# #+LaTeX_HEADER: \usepackage{ctable}
#+LaTeX_HEADER: \usepackage{multirow}

#+LaTeX_HEADER: \usepackage{array}
#+LaTeX_HEADER: \graphicspath{{./figures/}}

#+LaTeX_HEADER: \newcommand\petit{\fontsize{8}{0}\selectfont}
# #+BIBLIOGRAPHY: refs.bib

#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{fontsize=\scriptsize}
#+latex_header_extra: \setminted{fontsize=\scriptsize, linenos, breaklines}

* Introduction
** A word about me

#+ATTR_BEAMER: :overlay +-
- CV onliner: Prépa MP - ENSIIE (HPC) - PhD
- My experience with Rust: mostly fun projects
  - Molecular dynamics simulation (school project)
  - Ray Tracer
  - Text viewer
  - Roguelike

** Why Rust?
#+BEAMER: \pause

#+BEAMER: \only<2> {
Cause of 912 high or critical security bugs affecting stable branch of Chromium

#+ATTR_LATEX: :width 0.7\textwidth
[[file:figures/security-bug-causes-chromium.png]]
#+BEAMER: }

#+BEAMER: \pause

#+ATTR_BEAMER: :overlay +-
- Creation: 2006 (Graydon Hoare, Mozilla)
- First stable release: May 15th, 2015
- Rust Foundation: 2021
- Multi-paradigm: OO, functional
- key features: memory-safety, type-safety, lack of data races, zero-cost abstraction

* Getting a feel of the language

** Variables

#+BEAMER: \pause

Variable declaration :
#+begin_src rust
let x;
x = 42;
let greetings = "Hello there!";
#+end_src

#+BEAMER: \pause

Explicit type :
#+begin_src rust
let mut y: i32 = 43;
#+end_src

#+BEAMER: \pause

- Integers: ~i8~, ~i16~, ~i32~, ~i64~, ~i128~
- Unsigned integers: ~u8~, ..., ~u128~
- Floating points: ~f32~, ~f64~
- Pointer-size unsigned integer: ~usize~

** Variables

*** /Me and my shadow on the wall/
#+begin_src rust
let x = 41;
let x = x + 1;
#+end_src



#+BEAMER: \pause

*** /I went through the desert on a variable with no name.../
#+begin_src rust
let _ = 42;
let _ = do_stuff();
#+end_src

** Variables

*** /What does the compiler say?/
#+begin_src
warning: unused variable: `greetings`
  --> src/main.rs:16:9
   |
16 |     let greetings = "Hello there!";
   |         ^^^^^^^^^ help: if this is intentional, prefix it with an
   |                         underscore: `_greetings`
   |
   = note: `#[warn(unused_variables)]` on by default
#+end_src

#+BEAMER: \pause

#+begin_src rust
let _greetings = "Hello there!";
#+end_src


** Tuples

*** /Tea for two and two for tea/
#+begin_src rust
let pair: (char, i32) = ('a', 42);
println!("({}, {})", pair.0, pair.1);
#+end_src

Stdout
#+begin_example
(a, 42)
#+end_example

Max length: 12
#+BEAMER: \pause

*** /Break them up/
#+begin_src rust
let (left, right) = greetings.split_at(5);
println!("({}, {})", left, right);
#+end_src

** Functions

*** /I will always return/
#+begin_src rust
fn greet() {
    println!("Hi there!");
}
#+end_src

Unit type: ~()~

#+begin_src rust
fn fair_dice_roll() -> i32 {
    4
}

fn fair_dice_roll() -> i32 {
    return 4;
}
#+end_src

** Blocks

#+begin_src rust
fn main() {
    let x = "out";
    {
        // this is a different `x`
        let x = "in";
        println!("{}", x);
    }
    println!("{}", x);
}
#+end_src

#+begin_example
in
out
#+end_example

** Conditions

#+begin_src rust
fn fair_dice_roll(feeling_lucky: bool) -> i32 {
    if feeling_lucky {
        6
    } else {
        4
    }
}
#+end_src

** Loops

*** For
#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src rust
for i in (0..10) {
    println!("{}", i*i);
}
#+end_src
#+BEAMER: \vskip-1ex
# #+BEAMER: \hspace{-2.5ex}

#+BEAMER: \pause

*** While
#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src rust
let mut number = 3;

while number != 0 {
    println!("{number}!");
    number -= 1;
}
#+end_src
#+BEAMER: \vskip-1ex

# #+BEAMER: \hspace{-2.5ex}

#+BEAMER: \pause

*** Loop
#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src rust
let mut number = 3;

loop {
    println!("{number}!");
    number -= 1;
    if number == 0 {
        break;
    }
}
#+end_src

#+BEAMER: \vfill
#+BEAMER: \vfill
#+BEAMER: \vfill
#+BEAMER: \vfill
#+BEAMER: \vfill
#+BEAMER: \vfill

** Vectors

*** /I'm friend with the vector that's under my bed/
#+begin_src rust
let monster = vec![0, 1, 2, 3];
for i in monster.iter() {
    println!("{}", i*i);
}
#+end_src

#+BEAMER: \pause

#+begin_src rust
let monster: Vec<i32> = Vec::new();
monster.push(0);
monster.push(1);
let monster = vec![0; 10]; // vector of length 10 containing only 0s
#+end_src

Methods: ~clear()~, ~pop()~, ~is_empty()~, ~len()~, ~sort()~, ~reverse()~...


** Custom types

*** Structs
#+begin_src rust
struct Point {
    x: f64,
    y: f64,
}

let origin = Point{ x:0.0,  y:0.0 };
println!("({}, {})", origin.x, origin.y);
#+end_src

** Custom types

*** Enums
#+begin_src rust
enum Species {
    H,
    C,
    N,
    O,
}

type Formula = Vec<(Species, u8)>;

let water: Formula = vec![(Species::H, 2), (Species::O, 1)];
#+end_src


** Useful enums

*** Options

#+begin_src rust
enum Option<T> {
    None,
    Some(T),
}
#+end_src

#+BEAMER: \pause

*** It's a match!
#+begin_src rust
let x: Option<i32> = get_stuff();
match x {
   Some(a) => println!("{}", a),
   None => println!("There is nothing there."),
}
#+end_src

#+BEAMER: \pause

Matches are always exhaustive !!!

** Useful enums

*** Result

#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src rust
enum Result<T, E> {
    Ok(T),
    Err(E),
}
#+end_src

#+BEAMER: \pause

*** Error handling 101 : Don't Panic!

#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src rust
use std::fs::File;

fn main() {
    let greeting_file = File::open("hello.txt"); // Result<File>

    let greeting_file = match greeting_file {
        Ok(file) => file,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    }; // File
}
#+end_src

#+BEAMER: \pause

Shortcut to automatically panic

#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src rust
let greeting_file = File::open("hello.txt").unwrap();
#+end_src



* Memory management

** Ownership rules

- Each value in Rust has an owner.
- There can only be one owner at a time.
- When the owner goes out of scope, the value will be dropped.

** The move

#+begin_src rust
let s1 = String::from("hello");
let s2 = s1;
println!("{}", s1);
#+end_src

#+BEAMER: \pause

#+begin_example
error[E0382]: borrow of moved value: `s1`
  --> src/main.rs:20:20
   |
17 |     let s1 = String::from("hello");
   |         -- move occurs because `s1` has type `String`,
   |            which does not implement the `Copy` trait
18 |     let s2 = s1;
   |              -- value moved here
19 |
20 |     println!("{}", s1);
   |                    ^^ value borrowed here after move
#+end_example

** The move

#+begin_src rust
fn takes_ownership(some_string: String) {
    println!("{}", some_string);
}

let s = String::from("hello");
takes_ownership(s);
#+end_src

** Borrowing

#+begin_src rust
fn change(some_string: &String) {
    some_string.push_str(", world");
}

let s = String::from("hello");
change(&s);
#+end_src

** Borrowing
#+begin_src rust
fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

let s = String::from("hello");
change(&mut s);
#+end_src

** Dangling

#+begin_src rust
fn dangle() -> &String {

    let s = String::from("hello");

    &s
}
#+end_src

#+BEAMER: \pause

#+begin_example
error[E0106]: missing lifetime specifier
...
error[E0515]: cannot return reference to local variable `s`
  --> src/main.rs:14:5
   |
14 |     &s
   |     ^^ returns a reference to data owned by the current function
#+end_example


* Advanced types
** You'll take another slice

#+begin_src rust
let s = String::from("Hello world!");
let hello: &str = &s[0..5];
let monster = vec![0, 1, 2, 3, 4, 5];
let monster_slice: &[i32] = &monster[3..];
#+end_src

#+BEAMER: \pause

#+begin_src rust
fn first_word(s: &str) -> &str {
    ...
}

...
first_word(&s);
#+end_src


** Traits

- A trait defines a behaviour i.e a group of functions
- A type implements a trait by implementing theses functions

#+BEAMER: \pause

*** Addition on overload
#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src rust
pub trait Add<Rhs = Self> {
    type Output;

    // Required method
    fn add(self, rhs: Rhs) -> Self::Output;
}

impl Add<Point> for Point {

    fn add(self, rhs: Self) -> Self {
        ...
    }
}

impl  Point {
        ...
}
#+end_src

** Traits

Wildely used traits you don't want to implement all the time:
- Copy
- Clone
- Display
- Debug
- PartialOrd
- Eq
- ...

** Traits

#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src rust
struct Point {
    x: f64,
    y: f64,
}

fn main() {
    let my_home = Point{ x: 16, y: 33 };
    println!("{:?}", my_home);
}
#+end_src

#+BEAMER: \pause

#+begin_example
error[E0277]: `Point` doesn't implement `Debug`
  --> src/main.rs:36:22
   |
36 |     println!("{:?}", my_home);
   |                      ^^^^^^^ `Point` cannot be formatted using `{:?}`
   |
   = help: the trait `Debug` is not implemented for `Point`
   = note: add `#[derive(Debug)]` to `Point` or manually `impl Debug for Point`
   = note: this error originates in the macro `$crate::format_args_nl` which comes from the expansion of the macro `println` (in Nightly builds, run with -Z macro-backtrace for more info)
help: consider annotating `Point` with `#[derive(Debug)]`
   |
19 + #[derive(Debug)]
20 | struct Point {
   |
#+end_example

** Traits

#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src rust
#[derive(Debug)]
struct Point {
    x: f64,
    y: f64,
}

fn main() {
    let my_home = Point{ x: 16, y: 33 };
    println!("{:?}", my_home);
}
#+end_src

#+begin_example
Point { x: 16.0, y: 33.0 }
#+end_example

** Traits

#+begin_src rust
trait Player {
    fn get_player_name(self) -> String;
    fn initialized(self, player_id: u8, first_move: Move, game_config: GameConfig);
    fn play_move(self, previous_move: Move, tile: Tile) -> Move;
    fn finalize(self);
}
#+end_src

#+BEAMER: \pause

#+begin_src rust
struct GMPlayer {
...
}

impl Player for GMPlayer {
...
}

fn register_player(player: &impl Player) {
...
}
#+end_src


** What's in the Box

- We need to know the size of a type. What about recursive types (e.g. linked lists)?
- Making copies of large data is long.
- I want a vector of structures that implement the Player trait they might be of different types.

#+BEAMER: \pause
*** Little boxes made of ticky-tacky
A Box<T> is a reference to data that has ownership of the data.

#+BEAMER: \pause
*** Linked list
#+begin_src rust
#[derive(Debug)]
enum List<T> {
    Cons(T, Box<List<T>>),
    Nil,
}
#+end_src

** What's in the Box

*** Huge vector
#+begin_src rust
let v = Box::new(Vec::<f64>::with_capacity(1000000));
#+end_src

*** Vector of Players

#+begin_src rust
let v: Vec<Box<dyn Player>> = Vec::new();
#+end_src

* The ecosystem

** Cargo the package management manager you would hire

*** Create projects
#+begin_src shell
cargo new my_project
tree my_project
#+end_src

#+BEAMER: \pause

#+begin_example
my_project
+- .git
+- src
|  +- main.rs
+- .gitignore
+- Cargo.toml
#+end_example

** Cargo the package management manager you would hire

*** Build and run and test with dependencies

#+begin_src shell
cargo build
cargo run
cargo test
#+end_src

#+BEAMER: \pause

*** Cargo.toml
#+ATTR_LATEX: :options fontsize=\tiny
#+begin_src toml
cargo-features = ["edition2021"]

[package]
name = "rust-tracer"
version = "0.1.0"
edition = "2018"

[dependencies]
rand = "0.8.4"
clap = { version = "3.2.14", features = ["derive"] }
nalgebra = "0.31.1"

[lib]
name = "tracer"
#+end_src

** Crates (rust libraries)

#+ATTR_LATEX: :width 0.5\textwidth
[[file:figures/crates-io.png]]

** The compiler is your mentor

- variable not initialized
- writing to an immutable variable
- value is never read
- use after move
- borrow after mutable borrow
- trait not implemented
- ...


* References
** References

*** For this presentation
- A half-hour to learn Rust: https://fasterthanli.me/articles/a-half-hour-to-learn-rust
- The Rust Programming Book: https://doc.rust-lang.org/book/
- Rust playground: https://play.rust-lang.org/?version=stable&mode=debug&edition=2021
- Chromium memory-safety: https://www.chromium.org/Home/chromium-security/memory-safety/

*** To learn and practice Rust
- CodinGames: https://www.codingame.com
- Advent of code: https://adventofcode.com/
- Learning Rust With Entirely Too Many Linked Lists: https://rust-unofficial.github.io/too-many-lists/
- Crust of Rust: https://www.youtube.com/playlist?list=PLqbS7AVVErFiWDOAVrPt7aYmnuuOLYvOa
- The Four Horsemen of Bad Rust Code: https://fosdem.org/2024/schedule/event/fosdem-2024-2434-the-four-horsemen-of-bad-rust-code/


* Backup
:PROPERTIES:
:UNNUMBERED:
:END:
** Lifetimes

#+begin_src rust
fn main() {
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {}", result);
}
#+end_src

#+BEAMER: \pause

#+begin_src rust
fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
#+end_src

** Lifetimes

#+begin_example
error[E0106]: missing lifetime specifier
 --> src/main.rs:9:33
  |
9 | fn longest(x: &str, y: &str) -> &str {
  |               ----     ----     ^ expected named lifetime parameter
  |
  = help: this function's return type contains a borrowed value,
          but the signature does not say whether it is borrowed
          from `x` or `y`
help: consider introducing a named lifetime parameter
  |
9 | fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
  |           ++++     ++          ++          ++
#+end_example

** Lifetimes

- Lifetime annotations don’t change how long any of the references live.
- Lifetime annotations describe the relationship of the lifetimes of multiple references.

#+BEAMER: \pause

#+begin_src rust
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
#+end_src

#+BEAMER: \pause

The returned reference will be valid as long as BOTH arguments are valid.

** Lifetimes

#+begin_src rust
fn main() {
    let string1 = String::from("long string is long");
    let result;
    {
        let string2 = String::from("xyz");
        result = longest(string1.as_str(), string2.as_str());
    }
    println!("The longest string is {}", result);
}
#+end_src


** Tests

#+begin_src rust
pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
#+end_src
